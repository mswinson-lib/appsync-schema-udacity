type Instructor {
  bio: String
  image: String
  name: String
}

type Course {
  title: String
  subtitle: String
  homepage: String
  level: String
  starter: String
  image: String
  bannerImage: String
  teaser_video: String
  summary: String
  short_summary: String
  required_knowledge: String
  expected_learning: String
  featured: String
  syllabus: String
  faq: String
  full_course_available: String
  expected_duration: String
  expected_duration_unit: String
  new_release: String
  affiliates: [String]
  instructors: [Instructor]
  related_degree_keys: [String]
}

type Degree {
  title: String
  subtitle: String
  homepage: String
  level: String
  starter:Boolean
  image: String
  bannerImage: String
  teaser_video: String
  summary: String
  short_summary: String
  required_knowledge: String
  expected_learning: String
  featured:Boolean
  syllabus: String
  faq: String
  full_course_available:Boolean
  expected_duration:Int
  expected_duration_unit: String
  new_release:Boolean
  affiliates: [String]
  instructors: [Instructor]
  project_name: String
  project_description: String
  slug: String
  tracks: [String]
}

type Track {
  name: String
  description: String
}
 
type Catalog {
  courses(title: String = null): [ Course ]
  degrees: [ Degree ]
  tracks: [ Track ]
}

type Api {
  catalog: Catalog
}

type Query {
  udacity: Api
}

schema {
  query: Query
}

